import boto3

dynamodb = boto3.resource('dynamodb')

def create_table():
    table = dynamodb.create_table(
        TableName='mytable',
        KeySchema=[ 
            {
                'AttributeName': 'username',
                'KeyType': 'HASH'
            },
            {
                'AttributeName': 'password',
                'KeyType': 'RANGE'
            }
        ],
        AttributeDefinitions=[
            {
                'AttributeName': 'user_email',
                'AttributeType': 'S'
            },
            {
                'AttributeName': 'password',
                'AttributeType': 'S'
            }
         ],
         ProvisionedThroughput={
             'ReadCapacityUnits': 1,
             'WriteCapacityUnits': 1
         }
    )
    return table.table_status


# def insert_data():
#     table = dynamodb.Table('share_details') 
#     #with put_item function we insert data in Table
#     response = table.put_item(
#         Item = {
#                'username': 'utkarsh@gmail.com',
#                'password': 'password',
#                'first_name':'Utkarsh'  
#                } 
#         )
#     return response

# def get_data():
#     table = dynamodb.Table('mytable')    
#     response = table.get_item(
#         Item = {
#                'username': 'utkarsh@gmail.com',
#                'password':'password'
#                }
#         )
#     return response['Item']



# def delete_data():
#     table = dynamodb.Table('mytable')
#     response = table.delete_item(
#         Item = {
#                'username': 'utkarsh@gmail.com',
#                'password':'password'
#                }
#         )
#     return response['Item']


# def update_data():
#     table = dynamodb.Table('mytable')
#     response = table.update_item(
#                 Key={
#                     'username':'utkarsh@gmail.com',
#                     'password': 'password'
#                     },
#                 UpdateExpression='SET fname = :values',
#                 ExpressionAttributeValues={
#                     ':values': 'Utkarsh Pateriya'
#                 }
#            )
#    return response['Item']